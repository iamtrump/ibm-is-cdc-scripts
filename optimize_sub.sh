#!/bin/bash
SUB=$1
if [ -z ${SUB} ]; then
  echo 'Help.'
  echo 'Usage: ./optimize_sub.sh subscription [ddl]'
  exit
fi
DDL=$2
current_task=1
# Multiprocessing options:
np=8
declare -a pids

process_table() {
  FILE=${1}
  i=${2}
  SUB=${3}
  DDL=${4}
  REPLACELIST=${5}

  TABLE_NAME=$(xpath ${FILE} "/TS/Subscription/TableMapping[$i]/@sourceTableName" 2>/dev/null | awk -F \" '{print $2}')
  echo Processing table \#${i}: ${TABLE_NAME}…
  COUNT_COLS=$(xpath ${FILE} "count(/TS/Subscription/TableMapping[$i]/SourceColumn)" 2>/dev/null)
  for ((j=1; j<=${COUNT_COLS}; j++)) do
    COL_NAME=$(xpath ${FILE} "/TS/Subscription/TableMapping[$i]/SourceColumn[$j]/@columnName" 2>/dev/null | awk -F \" '{print $2}')
    COL_ID=$(xpath ${FILE} "/TS/Subscription/TableMapping[$i]/SourceColumn[$j]/@id" 2>/dev/null | awk -F \" '{print $2}')
    COL_ID_DDL=$(grep "${TABLE_NAME}.${COL_NAME} " ${DDL} | awk '{print $2}')
    if [ -z ${COL_ID_DDL} ]; then
       echo ${TABLE_NAME}.${COL_NAME} doesn’t exist on source database.
       # delete from source
    elif [ ${COL_ID} -ne ${COL_ID_DDL} ]; then
      echo ${TABLE_NAME}.${COL_NAME} has ID ${COL_ID}. Must be ${COL_ID_DDL}. Adding to replace list...
      # adding to replace list:
      PATTERN_TABLE='sourceTableName="'${TABLE_NAME}'"'
      PATTERN_COLUMN='columnName="'${COL_NAME}'"'
      PATTERN_ID='id="'${COL_ID}'"'
      PATTERN_ID_DDL='id="'${COL_ID_DDL}'"'
      cat << EOF >> ${REPLACELIST}
      echo ${TABLE_NAME}.${COL_NAME} id ${COL_ID} → ${COL_ID_DDL} &&
      awk -v pattern_table='${PATTERN_TABLE}' -v pattern_column='${PATTERN_COLUMN}' -v pattern_id='${PATTERN_ID}' -v pattern_id_ddl='${PATTERN_ID_DDL}' '
       BEGIN {isourtable=0
              issourcecolumn=0
              isourcolumn=0}
       \$0 ~ pattern_table {isourtable=1}
       \$0 ~ /<\/TableMapping>/ {isourtable=0}
       \$0 ~ /<SourceColumn/ {issourcecolumn=1}
       \$0 ~ pattern_column {isourcolumn=1}
       \$0 ~ /\/>/ {issourcecolumn=0
                   isourcolumn=0}
       \$0 ~ pattern_id {if (isourtable==1 && issourcecolumn==1 && isourcolumn==1)
                          gsub(pattern_id, pattern_id_ddl, \$0)}
       {print}' ${SUB} > \${TMP} && cp \${TMP} ${SUB} && echo Done.
EOF
    fi
  done 
}

FILE=$(mktemp /tmp/XXXXXX)
# create light version of subscribtion:
grep "<\|>\|sourceTableName\|columnName\|id" ${SUB} > ${FILE}
COUNT_TABLES=$(xpath ${FILE} "count(/TS/Subscription/TableMapping)" 2>/dev/null)
case ${DDL} in
  '')
    # get list of source tables from subscription:
    echo '-- getddl.sql:'
    for ((i=1; i<=${COUNT_TABLES}; i++)) do
      xpath ${FILE} "/TS/Subscription/TableMapping[$i]/@sourceTableName" 2>/dev/null | awk -F \" '{print "select tabname||'\''.'\''||colname||'\'' '\''||(colno+1) from syscat.columns where tabname='\''" $2 "'\'';"}'
    done
    echo '--db2 -xtvf getddl.sql > ddl.txt'
  ;;
  *)
    # checking
    echo Total tables: ${COUNT_TABLES}
    REPLACELIST=$(mktemp /tmp/XXXXXX)
    echo Replace list: ${REPLACELIST}
    echo Number of threads: ${np}
    echo -e '#!/bin/bash\nTMP=$(mktemp /tmp/XXXXXX)' > ${REPLACELIST}
    echo
    while [ ${current_task} -le ${COUNT_TABLES} ]; do
      for i in $(seq 1 ${np}); do
        if [ ${current_task} -le ${COUNT_TABLES} ]; then
          process_table ${FILE} ${current_task} ${SUB} ${DDL} ${REPLACELIST} &
          pids[${i}]=$!
          ((current_task++))
        else
          unset pids[${i}]
        fi
      done
      wait ${pids[@]}
    done
    sh ${REPLACELIST}
  ;;
esac
