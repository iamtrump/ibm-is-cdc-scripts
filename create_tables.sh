#!/bin/sh
FILE=$1
COUNT_TABLES=$(xpath $FILE 'number(count(/TS/Subscription/TableMapping))' 2>/dev/null)
i=1
while [ $i -le $COUNT_TABLES ]
do
  printf "CREATE TABLE "
  TABLE_NAME=$(xpath $FILE "/TS/Subscription/TableMapping[$i]/@targetTableName" 2>/dev/null | awk -F \" '{print $2}')
  FIELD_COUNT=$(xpath $FILE "count(/TS/Subscription/TableMapping[$i]/TargetColumn)" 2>/dev/null)
  printf $TABLE_NAME" ("
  j=1
  while [ $j -le $FIELD_COUNT ]
  do
    COL_NAME=$(xpath $FILE "/TS/Subscription/TableMapping[$i]/TargetColumn[$j]/@columnName" 2>/dev/null | awk -F \" '{print $2}')
    COL_TYPE=$(xpath $FILE "/TS/Subscription/TableMapping[$i]/TargetColumn[$j]/@dataType" 2>/dev/null | awk -F \" '{print $2}')
    printf $COL_NAME" "$COL_TYPE
    if [ $COL_TYPE = "NVARCHAR" -o $COL_TYPE = "VARCHAR" ]; then
      LENGTH=$(xpath $FILE "/TS/Subscription/TableMapping[$i]/TargetColumn[$j]/@length" 2>/dev/null | awk -F \" '{print $2}') 
      printf "("$LENGTH")"
    fi
    if [ $j -lt $FIELD_COUNT ]; then printf ", "; fi
    j=$(( $j + 1 ))
  done

  echo ");"
  i=$(( $i + 1 ))
done
